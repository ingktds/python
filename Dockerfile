FROM centos:latest
LABEL maintainer="tadashi.1027@gmail.com"

ARG VERSION="3.7.0"

RUN yum -y install gcc make zlib-devel openssl-devel bzip2-devel ncurses-devel sqlite-devel readline-devel tk-devel gdbm-devel db4-devel libpcap-devel xz-devel expat-devel libffi-devel && \
    curl -O https://www.python.org/ftp/python/${VERSION}/Python-${VERSION}.tar.xz && \
    tar xJf Python-${VERSION}.tar.xz && \
    cd Python-${VERSION} && \
    ./configure && \
    make && \
    make altinstall && \
    cd ../ && \
    rm -rf Python-${VERSION}*

