# Docker image for python

# Build docker image

```
$ cd <WORK_DIR>/python
$ docker build -t <container_image_name> .
```

# Run docker image

```
$ cd <WORK_DIR>/python
$ docker run --rm <container_image_name> python3.7 -V
```
